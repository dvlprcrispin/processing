// -- CONSTANTS --
final int CHR = 16;
// -- SETTINGS --

// WORKS ONLY IF WIDTH CAN BE EVENLY DIVIDED BY DIVISOR
// final int WIDTH = 1920;
// final int HEIGHT = 1080;
final int WIDTH = 640;
final int HEIGHT = 480;

final color BG = color(#FFFFFF);

final int DIVISOR = 32;

final int SEED = 1864814909;

// RGB or HSB or CHR
final int COLOUR_MODE = CHR;

final String OUT_LOC = "output/####.png";
