void doSquares() {

    if (COLOUR_MODE == HSB) {
        colorMode(HSB);
    }

    // background(BG);
    
    int block_size = WIDTH / DIVISOR;

    float screen_ratio = (WIDTH / (HEIGHT * 1.0));
    // no idea
    int num_boxes = int(screen_ratio * DIVISOR * 0.5625 * DIVISOR);

    for (int i = 0; i < num_boxes; i++) {

        if (COLOUR_MODE == HSB) {
            int h = (SEED * i) % 256;
            int s = 64 + ((SEED * i) % 192);
            int b = 192;

            fill(h, s, b);
        } else if (COLOUR_MODE == RGB) {
            int r = 128 + ((SEED * i) % 128);
            int g = 128 + ((SEED * (i * 2)) % 128);
            int b =  128 + ((SEED * (i * 3)) % 128);

            fill(r, g, b);
        } else if (COLOUR_MODE == CHR) {
            int r = 128 + ((SEED * i) % 128);
            int g = 128 + ((SEED * (i * 2)) % 128);
            int b = 0;

             if (r > g){
                g = 0;
            } else {
                r = 0;
            }

            fill(r, g, b);
        }

        rect(i * block_size % WIDTH, (i / DIVISOR) * block_size, 
            block_size, block_size);
    }
}

void drawRightTriange(int originX, int originY, int base, int facing) {
    /*
        FACING
        0 - L
        1 - Γ
        2 - ˥
        3 - ⅃

    */

    int[] corner = {originX, originY};
    int[] top = new int[2];
    int[] side = new int[2];
    if (facing == 0) {
        top[0] = originX + base;
        top[1] = originY;

        side[0] = originX; 
        side[1] = originY - base;
    } else if (facing == 1) {
        top[0] = originX + base;
        top[1] = originY;
        
        side[0] = originX; 
        side[1] = originY + base;
    } else if (facing == 2) {
        top[0] = originX - base;
        top[1] = originY;
        
        side[0] = originX; 
        side[1] = originY + base;
    } else if (facing == 3) {
        top[0] = originX - base;
        top[1] = originY;
        
        side[0] = originX; 
        side[1] = originY - base;
    } 
    

    triangle(corner[0], corner[1],
            top[0], top[1],
            side[0], side[1]);

}